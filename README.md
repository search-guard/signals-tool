# Signals Tool

The `Signals Tool` is a command line remote control tool for Signals.

Currently, the `Signals Tool` is just an uncompleted technical preview. Occurring bugs or issues can be reported to the [GitLab Repository](https://git.floragunn.com/search-guard/signals-tool/).

## Getting Started

To use the `Signals Tool` you need the following prerequisites:

- Java 17 or newer
- A cluster with Signals installed
- Authentication credentials (username, password) for a user that has Signals access on the cluster

To establish a connection to the cluster execute the following command:

```shell
./signals connect --host <cluster-hostname> --user <username> --password <password>
```

If the cluster uses certificates which are signed by a private PKI, you can specify the root certificate of the PKI by using the parameter `--ca-cert`.

```shell
./signals connect --host <cluster-hostname> --user <username> --password <password> --ca-cert <path-to-certificate>
```

When executing this command the `Signals Tool` tries to connect to the cluster. If the connection is successful
the `Signals Tool` saves the connection profile to a local file that can be reused by further commands.
Therefor you don't have to specify the connection configuration for each command.

If you are actively using multiple configurations you can specify a custom profile name by using `--profile` to keep track
of all profiles:

```shell
./signals connect --profile <my-profile-name> --host <cluster-hostname> --user <username> --password <password> --ca-cert <path-to-certificate>
```

## Usage

### Uploading a Watch

To upload a watch start by copying the path to the json file that contains the watch. Use the following command to upload the watch with the copied path:

```shell
./signals update-watch <watch-name> --input <json-file-path>
```

In case you want the watch to be inactive after creation use the `--inactive` flag:

```shell
./signals update-watch <watch-name> --input <json-file-path> --inactive
```

If multi tenancy is enabled on the cluster you can use the `--tenant` option to specify the tenant:

```shell
./signals update-watch <watch-name> --input <json-file-path> --tenant <tenant-name>
```

This tenant option is also available for all other commands that operate on watches. If `--tenant` is not specified the default tenant `_main` is used.

### Listing Watches

To get an overview over all watches on the cluster you can use the `list-watches` command:

```shell
./signals list-watches <watch-name>
```

If you want to download a single watch use the `get-watch` command.

### Watch States

To keep track of the current overall state you can use the `list-states` command. It prints you an overview of watches and their state in form of table:

```shell
./signals list-states
```

For a more detailed state of a single watch you can use the `get-state` command:

```shell
./signals get-state <watch-name>
```

### Operate Watches

Acknowledging a watch can simply be done with the `ack-watch` command:

```shell
./signals ack-watch <watch-name>
```

This works similarly for activating and deactivating watches with the `activate-watch` and `deactivate-watch` commands.
