package com.floragunn.searchguard.signalstool;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.DocReader;
import com.floragunn.searchguard.test.GenericRestClient;
import com.floragunn.searchguard.test.helper.cluster.LocalCluster;
import com.floragunn.signals.SignalsModule;
import com.floragunn.signals.watch.Watch;
import com.floragunn.signals.watch.WatchBuilder;
import com.google.common.collect.Maps;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.xcontent.XContentType;
import org.junit.jupiter.api.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class SignalsToolIntegrationTest {
    private static LocalCluster CLUSTER;
    private static InetSocketAddress HTTP_ADDRESS;
    private static Path CONF_DIR;
    private static OutputValidator VALIDATOR;

    private static Watch SIMPLE_TEST_WATCH;
    private static String SIMPLE_TEST_WATCH_NAME = "simple-test-watch";
    private static GenericRestClient CLIENT;

    @BeforeAll
    public static void setup() throws Exception {
        CLUSTER = new LocalCluster.Builder().singleNode().resources("sg_config").sslEnabled().enableModule(SignalsModule.class).start();
        HTTP_ADDRESS = CLUSTER.getHttpAddress();
        CONF_DIR = Files.createTempDirectory("signals-tool-config-dir");
        SIMPLE_TEST_WATCH = new WatchBuilder(SIMPLE_TEST_WATCH_NAME).atInterval("100ms").search("testsource").query("{\"match_all\" : {} }")
                .as("testsearch2").build();
        CLIENT = CLUSTER.getRestClient("uhura", "uhura");
    }

    private static void deleteWatch(String name) throws Exception {
        var response = CLIENT.delete("/_signals/watch/_main/" + name);
        assertEquals(200, response.getStatusCode());
    }

    private static void putWatch(String name, String watch) throws Exception {
        var response = CLIENT.putJson("/_signals/watch/_main/" + name, watch);
        assertEquals(201, response.getStatusCode());
    }

    private static DocNode getWatch(String name) throws Exception {
        var response = CLIENT.get("/_signals/watch/_main/" + name);
        assertEquals(200, response.getStatusCode());
        return DocNode.wrap(DocReader.json().readObject(response.getBody())).getAsNode("_source").without("_meta").without("_tenant");
    }

    @BeforeEach
    public void connect() {
        int res = SignalsTool.exec("connect", "--insecure", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp", "--host",
                HTTP_ADDRESS.getHostString(), "--port", String.valueOf(HTTP_ADDRESS.getPort()), "--ca-cert",
                CLUSTER.getTestCertificates().getCaCertificate().getCertificateFile().getAbsolutePath(), "--user", "uhura", "--password", "uhura");
        assertEquals(0, res);
        VALIDATOR = new OutputValidator();
    }

    //TODO: @AfterEach: delete all watches so failing tests do not lead to other tests failing

    @Nested
    class ConnectCommand {
        @Test
        public void testConnectWithExistingProfile() throws Exception {
            int res = SignalsTool.exec("connect", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
            assertEquals(0, res);
            VALIDATOR.expectConnect(CLUSTER.getClusterName(), "uhura").validate();
        }

        @Test
        public void testConnectWithProfileOverride() throws Exception {
            int res = SignalsTool.exec("connect", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp", "--user", "noshirt",
                    "--password", "redshirt");
            assertEquals(0, res);
            VALIDATOR.expectConnect(CLUSTER.getClusterName(), "noshirt").expectProfileOverride("schnupp").validate();
        }

        @Test
        public void testConnectWithNewProfileWithGenericName() throws Exception {
            int res = SignalsTool.exec("connect", "--insecure", "--signals-tool-config-dir", CONF_DIR.toString(), "--host",
                    HTTP_ADDRESS.getHostString(), "--port", String.valueOf(HTTP_ADDRESS.getPort()), "--ca-cert",
                    CLUSTER.getTestCertificates().getCaCertificate().getCertificateFile().getAbsolutePath(), "--user", "uhura", "--password",
                    "uhura");
            assertEquals(0, res);
            VALIDATOR.expectConnect(CLUSTER.getClusterName(), "uhura")
                    .expectNewProfileSave(CLUSTER.getHttpAddress().getAddress().getHostAddress() + "_uhura", CONF_DIR.toString()).validate();
        }

        @Test
        public void testConnectWithMissingParameters() {
            int res = SignalsTool.exec("connect", "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidProfileSettings("--host", "--user", "--password").validate();
        }
    }

    @Nested
    class GetWatchCommand {
        @Test
        public void testGetWatch() throws Exception {
            putWatch(SIMPLE_TEST_WATCH_NAME, SIMPLE_TEST_WATCH.toJson());

            int res = SignalsTool.exec("get-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(0, res);
            Map<String, Object> expected = DocNode.wrap(DocReader.json().readObject(SIMPLE_TEST_WATCH.toJson())).without("_meta");
            Map<String, Object> actual = DocNode.wrap(DocReader.json().readObject(VALIDATOR.getOut()));
            Assertions.assertEquals(expected, actual, Maps.difference(expected, actual).toString());
            VALIDATOR.reset();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }

        @Test
        public void testGetNonExistingWatch() throws Exception {
            int res = SignalsTool.exec("get-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidResponse("Not found").validate();
        }

        @Test
        public void testGetWatchToFile() throws Exception {
            putWatch(SIMPLE_TEST_WATCH_NAME, SIMPLE_TEST_WATCH.toJson());
            Path dir = Files.createTempDirectory("my-test-watches");
            File file = new File(dir.toFile(), SIMPLE_TEST_WATCH_NAME + ".json");

            int res = SignalsTool.exec("get-watch", SIMPLE_TEST_WATCH_NAME, "--output", dir.toString(), "--signals-tool-config-dir",
                    CONF_DIR.toString());
            assertEquals(0, res);
            Map<String, Object> expected = DocNode.wrap(DocReader.json().readObject(SIMPLE_TEST_WATCH.toJson())).without("_meta");
            Map<String, Object> actual = DocNode.wrap(DocReader.json().readObject(file));
            Assertions.assertEquals(expected, actual, Maps.difference(expected, actual).toString());
            VALIDATOR.validate();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }

        @Test
        public void testGetWatchToDirectory() throws Exception {
            putWatch(SIMPLE_TEST_WATCH_NAME, SIMPLE_TEST_WATCH.toJson());
            Path file = Files.createTempFile("simple-watch", "json");

            int res = SignalsTool.exec("get-watch", SIMPLE_TEST_WATCH_NAME, "--output", file.toString(), "--signals-tool-config-dir",
                    CONF_DIR.toString());
            assertEquals(0, res);
            Map<String, Object> expected = DocNode.wrap(DocReader.json().readObject(SIMPLE_TEST_WATCH.toJson())).without("_meta");
            Map<String, Object> actual = DocNode.wrap(DocReader.json().readObject(file.toFile()));
            Assertions.assertEquals(expected, actual, Maps.difference(expected, actual).toString());
            VALIDATOR.validate();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }
    }

    @Nested
    class UpdateWatchCommand {
        @Test
        public void testUpdateNonExistingWatchFromJson() throws Exception {
            int res = SignalsTool.exec("update-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString(), "--json",
                    SIMPLE_TEST_WATCH.toJson());
            assertEquals(0, res);
            VALIDATOR.expectUpdateWatchCreated(SIMPLE_TEST_WATCH_NAME).validate();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }

        @Test
        public void testUpdateNonExistingInactiveWatchFromJson() throws Exception {
            int res = SignalsTool.exec("update-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString(), "--json",
                    SIMPLE_TEST_WATCH.toJson(), "--inactive");
            assertEquals(0, res);
            VALIDATOR.expectUpdateWatchCreated(SIMPLE_TEST_WATCH_NAME).validate();
            DocNode actualWatch = getWatch(SIMPLE_TEST_WATCH_NAME);
            assertTrue(actualWatch.hasNonNull("active"));
            assertFalse(actualWatch.getBoolean("active"));

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }

        @Test
        public void testUpdateExistingWatchFromFile() throws Exception {
            File watchFile = File.createTempFile(SIMPLE_TEST_WATCH_NAME, ".json");
            BufferedWriter writer = new BufferedWriter(new FileWriter(watchFile));
            writer.write(SIMPLE_TEST_WATCH.toJson());
            writer.close();

            int res = SignalsTool.exec("update-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString(), "--input",
                    watchFile.getPath(), "--inactive");
            assertEquals(0, res);
            VALIDATOR.expectUpdateWatchCreated(SIMPLE_TEST_WATCH_NAME).validate();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }

        @Test
        public void testUpdateWatch() throws Exception {
            putWatch(SIMPLE_TEST_WATCH_NAME, SIMPLE_TEST_WATCH.toJson());

            int res = SignalsTool.exec("update-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString(), "--json",
                    SIMPLE_TEST_WATCH.toJson());
            assertEquals(0, res);
            VALIDATOR.expectUpdateWatchUpdated(SIMPLE_TEST_WATCH_NAME).validate();

            deleteWatch(SIMPLE_TEST_WATCH_NAME);
        }
    }

    @Nested
    class DeleteWatchCommand {
        @Test
        public void testDeleteWatch() throws Exception {
            putWatch(SIMPLE_TEST_WATCH_NAME, SIMPLE_TEST_WATCH.toJson());

            int res = SignalsTool.exec("delete-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(0, res);
            VALIDATOR.expectDeleteWatch(SIMPLE_TEST_WATCH_NAME, "_main").validate();

            GenericRestClient.HttpResponse response = CLIENT.get("/_signals/watch/_main/" + SIMPLE_TEST_WATCH_NAME);
            assertEquals(404, response.getStatusCode());
        }

        @Test
        public void testDeleteNonExistingWatch() throws Exception {
            int res = SignalsTool.exec("delete-watch", SIMPLE_TEST_WATCH_NAME, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidResponse("Not Found").validate();
        }
    }

    @Nested
    class ListWatchesCommand {
        @Test
        public void testListWatchesNoWatches() throws Exception {
            int res = SignalsTool.exec("list-watches", "--signals-tool-config-dir", CONF_DIR.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.expectListWatches().validate();
        }

        @Test
        public void testListWatches() throws Exception {
            for (int i = 0; i < 3; i++) {
                putWatch("watch" + i, new WatchBuilder("watch" + i).build().toJson());
            }
            putWatch("watch3", new WatchBuilder("watch3").inactive().build().toJson());

            int res = SignalsTool.exec("list-watches", "--signals-tool-config-dir", CONF_DIR.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.expectListWatches(new OutputValidator.ListWatchesOutput("watch0", true), new OutputValidator.ListWatchesOutput("watch1", true),
                    new OutputValidator.ListWatchesOutput("watch2", true), new OutputValidator.ListWatchesOutput("watch3", false)).validate();

            for (int i = 0; i < 4; i++) {
                deleteWatch("watch" + i);
            }
        }
    }

    @Nested
    class ActivateWatchCommand {
        @Test
        public void testActivateWatch() throws Exception {
            String inactiveWatchName = "inactive";
            Watch inactiveWatch = new WatchBuilder(inactiveWatchName).inactive().build();
            putWatch(inactiveWatchName, inactiveWatch.toJson());

            int res = SignalsTool.exec("activate-watch", inactiveWatchName, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(0, res);
            VALIDATOR.expectActivateWatch(inactiveWatchName, "_main").validate();
            DocNode actual = getWatch(inactiveWatchName);
            assertTrue(actual.hasNonNull("active"));
            assertTrue(actual.getBoolean("active"));

            deleteWatch(inactiveWatchName);
        }

        @Test
        public void testActivateNonExistentWatch() throws Exception {
            int res = SignalsTool.exec("activate-watch", "non-existent", "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidResponse("Not Found");
        }
    }

    @Nested
    class DeactivateWatchCommand {
        @Test
        public void testDeactivateWatch() throws Exception {
            String activeWatchName = "active";
            Watch activeWatch = new WatchBuilder(activeWatchName).build();
            putWatch(activeWatchName, activeWatch.toJson());

            int res = SignalsTool.exec("deactivate-watch", activeWatchName, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(0, res);
            VALIDATOR.expectDeactivateWatch(activeWatchName, "_main").validate();
            DocNode actual = getWatch(activeWatchName);
            assertTrue(actual.hasNonNull("active"));
            assertFalse(actual.getBoolean("active"));

            deleteWatch(activeWatchName);
        }
    }

    @Nested
    class AcknowledgeWatchCommand {
        @Test
        public void testAcknowledgeWatch() throws Exception {
            CLUSTER.getInternalNodeClient().admin().indices().create(new CreateIndexRequest("testsource_ack_watch")).actionGet();
            CLUSTER.getInternalNodeClient().admin().indices().create(new CreateIndexRequest("testsink_ack_watch")).actionGet();
            CLUSTER.getInternalNodeClient().index(new IndexRequest("testsource_ack_watch").id("1")
                    .setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).source(XContentType.JSON, "key1", "val1", "key2", "val2")).actionGet();
            String watchName = "ack-watch";
            Watch watch = new WatchBuilder(watchName).atMsInterval(100).search("testsource_ack_watch").query("{\"match_all\" : {} }").as("testsearch")
                    .checkCondition("data.testsearch.hits.hits.length > 0").then().index("testsink_ack_watch")
                    .refreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).throttledFor("0").name("testaction").build();
            putWatch(watchName, watch.toJson());
            Thread.sleep(300);

            int res = SignalsTool.exec("ack-watch", watchName, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(0, res);
            VALIDATOR.validate();

            CLUSTER.getInternalNodeClient().admin().indices().delete(new DeleteIndexRequest("testsource_ack_watch")).actionGet();
            CLUSTER.getInternalNodeClient().admin().indices().delete(new DeleteIndexRequest("testsink_ack_watch")).actionGet();
            deleteWatch(watchName);
        }

        @Test
        public void testAcknowledgeNonExistentWatch() {
            int res = SignalsTool.exec("ack-watch", "unknown-watch", "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidResponse("Could not find watch").validate();
        }

        @Test
        public void testAcknowledgeUnacknowledgableWatch() throws Exception {
            String watchName = "unackable-watch";
            Watch watch = new WatchBuilder(watchName).build();
            putWatch(watchName, watch.toJson());
            Thread.sleep(300);

            int res = SignalsTool.exec("ack-watch", watchName, "--signals-tool-config-dir", CONF_DIR.toString());
            assertEquals(1, res);
            VALIDATOR.expectInvalidResponse("No actions are in an acknowlegable state").validate();

            deleteWatch(watchName);
        }
    }

    @Nested
    class GetSettingsCommand {
        @Test
        public void testGetAllEmptySettings() throws Exception {
            int res = SignalsTool.exec("get-setting", "--signals-tool-config-dir", CONF_DIR.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.expectEqualJson(DocNode.wrap(DocNode.EMPTY)).validate();
        }

        @Test
        public void testGetSetting() throws Exception {
            var response = CLIENT.putJson("/_signals/settings/tenant._main.active", "true");
            assertEquals(200, response.getStatusCode());

            int res = SignalsTool.exec("get-setting", "tenant._main.active", "--signals-tool-config-dir", CONF_DIR.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.expectOut("true\n").validate();

            response = CLIENT.delete("/_signals/settings/tenant._main.active");
            assertEquals(200, response.getStatusCode());
        }

        @Test
        public void testGetSettingsToFile() throws Exception {
            File settingsFile = File.createTempFile("new-signals-settings", ".json");

            int res = SignalsTool.exec("get-setting", "--signals-tool-config-dir", CONF_DIR.toString(), "--output", settingsFile.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.expectEqualJson(DocNode.wrap(DocNode.EMPTY)).validate();
            assertEquals(DocNode.wrap(DocNode.EMPTY), DocNode.wrap(DocReader.json().readObject(settingsFile)));
        }
    }

    @Nested
    class UpdateSettingsCommand {
        @Test
        public void testUpdateSetting() throws Exception {
            int res = SignalsTool.exec("update-setting", "tenant._main.active", "--signals-tool-config-dir", CONF_DIR.toString(), "--json", "true");
            Assertions.assertEquals(0, res);
            VALIDATOR.validate();

            var response = CLIENT.delete("/_signals/settings/tenant._main.active");
            assertEquals(200, response.getStatusCode());
        }
    }

    @Nested
    class DeleteSettingsCommand {
        @Test
        public void testDeleteSetting() throws Exception {
            var response = CLIENT.putJson("/_signals/settings/tenant._main.active", "true");
            assertEquals(200, response.getStatusCode());

            int res = SignalsTool.exec("delete-setting", "tenant._main.active", "--signals-tool-config-dir", CONF_DIR.toString());
            Assertions.assertEquals(0, res);
            VALIDATOR.validate();

            response = CLIENT.get("/_signals/settings");
            assertEquals(DocNode.wrap(DocNode.EMPTY), DocNode.wrap(DocReader.json().readObject(response.getBody())));
        }
    }
}