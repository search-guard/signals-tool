package com.floragunn.searchguard.signalstool;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.DocReader;
import com.floragunn.codova.documents.DocumentParseException;
import com.floragunn.codova.documents.UnexpectedDocumentStructureException;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.codova.validation.errors.MissingAttribute;
import com.google.common.collect.Maps;
import org.apache.commons.io.output.TeeOutputStream;
import org.junit.jupiter.api.Assertions;
import picocli.CommandLine;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OutputValidator {
    private final PrintStream stdOut = System.out;
    private final PrintStream stdErr = System.err;

    private final ByteArrayOutputStream outStreamCaptor = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errStreamCaptor = new ByteArrayOutputStream();

    private final StringBuilder outBuilder = new StringBuilder();
    private final StringBuilder errBuilder = new StringBuilder();
    private final List<JsonComparison> jsonComparisons = new ArrayList<>();

    public OutputValidator() {
        System.setOut(new PrintStream(new TeeOutputStream(outStreamCaptor, stdOut)));
        System.setErr(new PrintStream(new TeeOutputStream(errStreamCaptor, stdErr)));
    }

    public void setup() {
        System.setOut(new PrintStream(new TeeOutputStream(outStreamCaptor, stdOut)));
        System.setErr(new PrintStream(new TeeOutputStream(errStreamCaptor, stdErr)));
    }

    public OutputValidator expectOut(String expected) {
        outBuilder.append(expected);
        return this;
    }

    public OutputValidator expectErr(String expected) {
        errBuilder.append(expected);
        return this;
    }

    public String getOut() {
        return outStreamCaptor.toString();
    }

    public String getErr() {
        return errStreamCaptor.toString();
    }

    public OutputValidator expectUpdateWatchCreated(String watchName) {
        return expectOut("Successfully created watch '" + watchName + "'\n");
    }

    public OutputValidator expectUpdateWatchUpdated(String watchName) {
        return expectOut("Successfully updated watch '" + watchName + "'\n");
    }

    public OutputValidator expectDeleteWatch(String watchName, String tenant) {
        return expectOut("Successfully deleted watch '" + watchName + "' on tenant '" + tenant + "'\n");
    }

    public OutputValidator expectConnect(String clusterName, String userName) {
        return expectOut("Successfully connected to cluster '" + clusterName + "' with user '" + userName + "'\n");
    }

    public OutputValidator expectNewProfileSave(String profileName, String configDirPath) {
        return expectOut("Saving new profile " + profileName + " to " + configDirPath + "\n");
    }

    public OutputValidator expectProfileOverride(String profile) {
        return expectOut("Overwriting profile " + profile + "\n");
    }

    public OutputValidator expectInvalidProfileSettings(String... missingAttributes) {
        ValidationErrors valErrs = new ValidationErrors();
        for (String attr : missingAttributes) {
            valErrs.add(new MissingAttribute(attr));
        }
        try {
            valErrs.throwExceptionForPresentErrors();
        } catch (ConfigValidationException e) {
            expectErr(new SignalsTool.ToolException("Invalid profile settings", e).getMessage());
            expectErr("\n");
        }
        return this;
    }

    public OutputValidator expectListWatches(ListWatchesOutput... outputs) {
        expectOut(String.format("%-30s %-30s%n", "name", "active"));
        expectOut("-".repeat(60) + "\n");
        for (ListWatchesOutput output : outputs) {
            expectOut(String.format("%-30s %-30s%n", output.name, output.active));
        }
        return this;
    }

    public OutputValidator expectActivateWatch(String watchName, String tenant) {
        return expectOut("Activated watch '" + watchName + "' on tenant '" + tenant + "'\n");
    }

    public OutputValidator expectDeactivateWatch(String watchName, String tenant) {
        return expectOut("Deactivated watch '" + watchName + "' on tenant '" + tenant + "'\n");
    }

    public OutputValidator expectEqualJson(Map<String, Object> expected) throws UnexpectedDocumentStructureException, DocumentParseException {
        Map<String, Object> actual = DocNode.wrap(DocReader.json().readObject(getOut()));
        jsonComparisons.add(new JsonComparison(expected, actual));
        clear();
        return this;
    }

    public OutputValidator expectGetState(String name, String tenant, String severity, String status, String statusDetail,
            ActionStateOutput... actionStateOutputs) {
        expectOut(String.format("%-15s %s%n", "Name:", name));
        expectOut(String.format("%-15s %s%n", "Tenant:", tenant));
        expectOut(String.format("%-15s %s%n", "Severity:", severity == null ? "not configured" : severity));
        expectOut(String.format("%-15s %s%n", "Status:", status));
        if (statusDetail != null) {
            expectOut(String.format("%-15s %s%n", "Status detail:", statusDetail));
        }
        if (actionStateOutputs.length == 0) {
            expectOut(String.format("%-15s %s%n", "Actions:", "none"));
        } else {
            expectOut(String.format("%-15s %n", "Actions:"));
            for (ActionStateOutput output : actionStateOutputs) {
                expectOut(" ".repeat(4) + String.format("%-15s %n", output.name() + ":"));
                expectOut(" ".repeat(8)
                        + String.format("%-15s %s%n", "Last triggered:", output.lastTriggered() == null ? "unknown" : output.lastTriggered()));
                expectOut(" ".repeat(8) + String.format("%-15s %s%n", "Acknowledged:", output.acked()));
                expectOut(" ".repeat(8) + String.format("%-15s %s%n", "Status:", output.status() == null ? "unknown" : output.status()));
                if (output.statusDetail() != null) {
                    expectOut(" ".repeat(8) + String.format("%-15s %s%n", "Status detail:", output.statusDetail()));
                }
            }
        }
        return this;
    }

    public record ActionStateOutput(String name, String lastTriggered, boolean acked, String status, String statusDetail) {
    }

    public OutputValidator expectListStates(StateListRowOutput... rows) {
        expectOut("status" + " ".repeat(30 - "status".length()) + "name" + " ".repeat(30 - "name".length()) + "actions"
                + " ".repeat(30 - "actions".length()) + "\n");
        expectOut("-".repeat(90) + "\n");
        for (StateListRowOutput row : rows) {
            expectOut(row.status.toString() + " ".repeat(30 - row.status.getCJKAdjustedLength()) + row.name.toString()
                    + " ".repeat(30 - row.name.getCJKAdjustedLength()) + row.action.toString() + " ".repeat(30 - row.action.getCJKAdjustedLength())
                    + "\n");
        }
        return this;
    }

    public record StateListRowOutput(CommandLine.Help.Ansi.Text status, CommandLine.Help.Ansi.Text name, CommandLine.Help.Ansi.Text action) {
    }

    private record JsonComparison(Map<String, Object> expected, Map<String, Object> actual) {
        public void validate() {
            Assertions.assertEquals(expected, actual, Maps.difference(expected, actual).toString());
        }
    }

    public record ListWatchesOutput(String name, boolean active) {
    }

    public OutputValidator expectInvalidResponse(String reason) {
        expectErr("Invalid response from server: " + reason + "\n");
        return this;
    }

    private void clear() {
        outStreamCaptor.reset();
        errStreamCaptor.reset();
        outBuilder.setLength(0);
        errBuilder.setLength(0);
    }

    public void reset() {
        clear();
        jsonComparisons.clear();
    }

    public void validate() {
        Assertions.assertEquals(outBuilder.toString(), outStreamCaptor.toString());
        Assertions.assertEquals(errBuilder.toString(), errStreamCaptor.toString());
        for (JsonComparison comparison : jsonComparisons) {
            comparison.validate();
        }
        reset();
    }
}
