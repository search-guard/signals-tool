package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.OutputValidator;
import com.floragunn.searchguard.signalstool.SignalsTool;
import com.floragunn.searchguard.test.helper.certificate.TestCertificate;
import com.floragunn.searchguard.test.helper.certificate.TestCertificates;
import com.floragunn.signals.execution.WatchExecutionContextData;
import com.floragunn.signals.watch.result.Status;
import com.floragunn.signals.watch.severity.SeverityLevel;
import com.floragunn.signals.watch.severity.SeverityMapping;
import com.floragunn.signals.watch.state.ActionState;
import com.floragunn.signals.watch.state.WatchState;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.google.common.collect.ImmutableMap;
import org.apache.http.entity.ContentType;
import org.elasticsearch.common.Strings;
import org.elasticsearch.xcontent.ToXContentObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import picocli.CommandLine;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StateCommandsTest {
    private final static TestCertificates testCertificates = TestCertificates.builder().ca("CN=localhost,OU=SearchGuard,O=SearchGuard").build();
    private final static TestCertificate wmCert = testCertificates.create("CN=localhost,OU=SearchGuard,O=SearchGuard");

    @RegisterExtension
    public final static WireMockExtension WM = WireMockExtension
            .newInstance().options(wireMockConfig().dynamicHttpsPort().keystorePath(wmCert.getJksFile().getPath())
                    .keystorePassword(wmCert.getPrivateKeyPassword()).keyManagerPassword(wmCert.getPrivateKeyPassword()).needClientAuth(false))
            .build();

    private static Path CONF_DIR;
    private static OutputValidator VALIDATOR;

    @BeforeAll
    public static void connect() throws Exception {
        System.out.println(WM.baseUrl());
        CONF_DIR = Files.createTempDirectory("signals-tool-config-dir");
        WM.stubFor(get(urlEqualTo("/_searchguard/authinfo"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody("""
                        {
                            "user_name": "test-user",
                            "cluster_name": "test_cluster"
                        }
                        """)));
        int res = SignalsTool.exec("connect", "--insecure", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp", "--host",
                "localhost", "--port", String.valueOf(WM.getHttpsPort()), "--ca-cert",
                testCertificates.getCaCertificate().getCertificateFile().getPath(), "--user", "test-user", "--password", "test-password");
        assertEquals(0, res);
    }

    @BeforeEach
    public void resetValidator() {
        VALIDATOR = new OutputValidator();
    }

    public static void createWatchStateGetStub(String watchName, WatchState state) {
        WM.stubFor(get(urlEqualTo("/_signals/watch/" + state.getTenant() + "/" + watchName + "/_state")).willReturn(
                aResponse().withStatus(200).withHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()).withBody(state.toJsonString())));
    }

    public static void createWatchStateSearchStub(Map<String, WatchState> states, int hits) {
        String res = Strings.toString((ToXContentObject) (builder, params) -> {
            builder.startObject();
            builder.startObject("hits");
            builder.startObject("total");
            builder.field("value", hits);
            builder.endObject();
            builder.startArray("hits");
            for (Map.Entry<String, WatchState> entry : states.entrySet()) {
                builder.startObject();
                builder.field("_id", entry.getValue().getTenant() + "/" + entry.getKey());
                builder.field("_source", entry.getValue());
                builder.endObject();
            }
            builder.endArray();
            builder.endObject();
            builder.endObject();
            return builder;
        });
        WM.stubFor(post(urlPathMatching("/_signals/watch/" + states.values().stream().findFirst().get().getTenant() + "/_search/_state"))
                .withQueryParam("size", matching("^[-+]?\\d+$"))
                .willReturn(aResponse().withStatus(200).withHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()).withBody(res)));
    }

    public static void createWatchStateSearchStub(Map<String, WatchState> states) {
        createWatchStateSearchStub(states, states.size());
    }

    @Test
    public void testGetEmptyState() {
        WatchState state = new WatchState("_main");
        createWatchStateGetStub("test-watch", state);

        int res = SignalsTool.exec("get-state", "test-watch", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);

        VALIDATOR.expectGetState("test-watch", "_main", null, "unknown", null).validate();
    }

    @Test
    void testGetStateWithoutSeverity() {
        WatchState state = new WatchState("_main");
        state.setLastStatus(new Status(Status.Code.ACTION_EXECUTED, "Action executed"));
        ActionState actionState = state.getActionState("test-action");
        actionState.setLastStatus(new Status(Status.Code.ACTION_EXECUTED, null));
        createWatchStateGetStub("test-watch", state);

        int res = SignalsTool.exec("get-state", "test-watch", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);

        VALIDATOR.expectGetState("test-watch", "_main", null, "ACTION EXECUTED", "Action executed",
                new OutputValidator.ActionStateOutput("test-action", "unknown", false, "ACTION EXECUTED", null)).validate();

        state = new WatchState("_main");
        state.setLastStatus(new Status(Status.Code.SIMULATED_ACTION_EXECUTED, null));
        actionState = state.getActionState("test-action");
        actionState.setLastStatus(new Status(Status.Code.SIMULATED_ACTION_EXECUTED, null));
        createWatchStateGetStub("test-watch", state);
        res = SignalsTool.exec("get-state", "test-watch", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);
        VALIDATOR.expectGetState("test-watch", "_main", null, "SIMULATED ACTION EXECUTED", null,
                new OutputValidator.ActionStateOutput("test-action", "unknown", false, "SIMULATED ACTION EXECUTED", null)).validate();
    }

    @Test
    public void testListStates() {
        System.setProperty("picocli.ansi", "true");
        WatchState state1 = new WatchState("_main");
        WatchState state2 = new WatchState("_main");

        createWatchStateSearchStub(ImmutableMap.of("watch1", state1, "watch2", state2));
        int res = SignalsTool.exec("list-states", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);
        var tmp = CommandLine.Help.Ansi.ON;
        VALIDATOR.expectListStates(new OutputValidator.StateListRowOutput(tmp.text("Unknown"), tmp.text("watch1"), tmp.text("")),
                new OutputValidator.StateListRowOutput(tmp.text("Unknown"), tmp.text("watch2"), tmp.text(""))).validate();

        state1 = new WatchState("_main");
        state1.setLastStatus(new Status(Status.Code.EXECUTION_FAILED, "some details"));
        WatchExecutionContextData data = new WatchExecutionContextData();
        data.setSeverity(new SeverityMapping.EvaluationResult(SeverityLevel.ERROR,
                new SeverityMapping.Element(BigDecimal.valueOf(100), SeverityLevel.ERROR), 200));
        state1.setLastExecutionContextData(new WatchExecutionContextData());
        state1.getActionState("some-action").setLastStatus(new Status(Status.Code.ACTION_FAILED, null));
        state2 = new WatchState("_main");
        state2.setLastStatus(new Status(Status.Code.NO_ACTION, null));
        state2.getActionState("other-action").setLastStatus(new Status(Status.Code.ACTION_EXECUTED, null));

        createWatchStateSearchStub(ImmutableMap.of("watch1", state1, "watch2-with-really-really-long-name", state2));
        res = SignalsTool.exec("list-states", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);

        VALIDATOR.expectListStates(new OutputValidator.StateListRowOutput(tmp.text("@|fg(244) Failed|@"), tmp.text("watch1"), tmp.text("")),
                new OutputValidator.StateListRowOutput(tmp.text("@|green Active|@"), tmp.text("watch2-with-really-really-..."),
                        tmp.text("@|bold other-action|@")))
                .validate();

        state1 = new WatchState("_main");
        state1.setLastStatus(new Status(Status.Code.ACTION_EXECUTED, null));

        createWatchStateSearchStub(ImmutableMap.of("watch1", state1));
        res = SignalsTool.exec("list-states", "--signals-tool-config-dir", CONF_DIR.toString(), "--profile", "schnupp");
        assertEquals(0, res);

        VALIDATOR.expectListStates(new OutputValidator.StateListRowOutput(tmp.text("@|yellow Action|@"), tmp.text("watch1"), tmp.text("")))
                .validate();
    }
}
