package com.floragunn.searchguard.signalstool;

import com.floragunn.codova.config.net.TLSConfig;
import com.floragunn.codova.documents.*;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.codova.validation.errors.MissingAttribute;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.commands.*;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.http.HttpHost;
import org.apache.http.auth.UsernamePasswordCredentials;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import java.io.File;
import java.io.IOException;
import java.io.Serial;
import java.net.SocketException;
import java.util.LinkedHashMap;
import java.util.Map;

@Command(name = "signals", subcommands = { AcknowledgeWatch.class, ActivateWatch.class, Connect.class, DeactivateWatch.class, DeleteSettings.class,
        GetSettings.class, DeleteWatch.class, GetState.class, GetWatch.class, Licenses.class, ListStates.class, ListWatches.class,
        UpdateSettings.class, UpdateWatch.class })
public class SignalsTool {

    public static int exec(String... args) {
        return new CommandLine(new SignalsTool()).execute(args);
    }

    public static void main(String... args) {
        System.exit(exec(args));
    }

    public record Profile(String hostname, int port, TLSConfig tlsConfig, BasicAuthConfig basicAuthConfig) implements Document<Profile> {

        public static boolean profileExists(File confDir, String profileName) {
            if (profileName == null || confDir == null) {
                return false;
            }
            return new File(confDir, "profile_" + profileName + ".yml").exists();
        }

        public static String readSelectedProfileName(File confDir) throws ToolException {
            File selectedProfileFile = new File(confDir, "selected_profile.txt");
            if (selectedProfileFile.exists()) {
                try {
                    return Files.asCharSource(selectedProfileFile, Charsets.UTF_8).readFirstLine();
                } catch (IOException e) {
                    throw new ToolException("Error while reading " + selectedProfileFile, e);
                }
            }
            return null;
        }

        public static void writeSelectedProfileName(File confDir, String name) throws ToolException {
            File selectedProfileFile = new File(confDir, "selected_profile.txt");
            try {
                Files.asCharSink(selectedProfileFile, Charsets.UTF_8).write(name);
            } catch (IOException e) {
                throw new ToolException("Error while writing " + selectedProfileFile);
            }
        }

        public static void deleteSelectedProfileName(File confDir) throws ToolException {
            File selectedProfileFile = new File(confDir, "selected_profile.txt");
            if (selectedProfileFile.exists() && !selectedProfileFile.delete()) {
                throw new ToolException("Error while deleting " + selectedProfileFile);
            }
        }

        @Override
        public Object toBasicObject() {
            Map<String, Object> res = new LinkedHashMap<>();
            res.put("hostname", hostname);
            res.put("port", port);
            res.put("tls", tlsConfig.toBasicObject());
            res.put("basic_auth", basicAuthConfig.toBasicObject());
            return res;
        }

        public HttpHost toHttpHost() {
            return new HttpHost(hostname, port, "https");
        }

        public void write(File confDir, String name) throws ToolException {
            if (!confDir.exists() && !confDir.mkdir()) {
                throw new ToolException("Could not create directory " + confDir + ")");
            }
            String fileName = name == null ? hostname : name;
            File profileFile = new File(confDir, "profile_" + fileName + ".yml");
            try {
                DocWriter.yaml().write(profileFile, toBasicObject());
            } catch (IOException e) {
                throw new ToolException("Could not write profile " + profileFile + ")", e);
            }
        }

        public record BasicAuthConfig(String username, String password) implements Document<BasicAuthConfig> {
            @Override
            public Object toBasicObject() {
                Map<String, Object> res = new LinkedHashMap<>();
                res.put("username", username);
                res.put("password", password);
                return res;
            }

            public UsernamePasswordCredentials toCredentials() {
                return new UsernamePasswordCredentials(username, password);
            }
        }

        public static class Builder {
            private final File confDir;
            private String hostname = null;
            private Integer port = null;
            private boolean customPort = false;
            private String username = null;
            private String password = null;
            private File caCert = null;
            private boolean insecure = false;
            private boolean customInsecure = false;
            private TLSConfig tlsConfig = null;
            private BasicAuthConfig basicAuthConfig = null;

            public Builder(File confDir) {
                this.confDir = confDir;
            }

            public Builder hostname(String hostname) {
                this.hostname = hostname;
                return this;
            }

            public Builder port(Integer port) {
                this.port = port;
                return this;
            }

            public Builder customPort(boolean custom) {
                customPort = custom;
                return this;
            }

            public Builder username(String username) {
                this.username = username;
                return this;
            }

            public Builder password(String password) {
                this.password = password;
                return this;
            }

            public Builder caCert(File caCert) {
                this.caCert = caCert;
                return this;
            }

            public Builder insecure(boolean insecure) {
                this.insecure = insecure;
                return this;
            }

            public Builder customInsecure(boolean custom) {
                customInsecure = custom;
                return this;
            }

            private TLSConfig readTlsConfig(DocNode tlsConfigDoc) throws ConfigValidationException {
                Map<String, Object> tlsConfigMap = TLSConfig.parse(tlsConfigDoc).toBasicObject();
                if (caCert != null) {
                    tlsConfigMap.remove("trusted_cas");
                    tlsConfigMap.put("trusted_cas", "#{file:" + caCert.getAbsolutePath() + "}");
                }
                if (customInsecure) {
                    tlsConfigMap.remove("verify_hostnames");
                    tlsConfigMap.put("verify_hostnames", !insecure);
                }
                return TLSConfig.parse(tlsConfigMap);
            }

            protected BasicAuthConfig readBasicAuthConfig(DocNode basicAuthConfigDoc) throws ConfigValidationException {
                ValidationErrors validationErrors = new ValidationErrors();
                ValidatingDocNode vBasicAuthDoc = new ValidatingDocNode(basicAuthConfigDoc, validationErrors);
                String username = vBasicAuthDoc.get("username").required().asString();
                String password = vBasicAuthDoc.get("password").withDefault((String) null).asString();
                validationErrors.throwExceptionForPresentErrors();
                if (this.username == null) {
                    this.username = username;
                }
                if (this.password == null) {
                    this.password = password;
                }
                return new BasicAuthConfig(this.username, this.password);
            }

            private void read(DocNode profileDoc) throws ConfigValidationException {
                ValidationErrors validationErrors = new ValidationErrors();
                ValidatingDocNode vProfileConfigDoc = new ValidatingDocNode(profileDoc, validationErrors);
                String hostname = vProfileConfigDoc.get("hostname").required().asString();
                int port = vProfileConfigDoc.get("port").required().asInt();
                try {
                    tlsConfig = readTlsConfig(vProfileConfigDoc.get("tls").required().asDocNode());
                    basicAuthConfig = readBasicAuthConfig(vProfileConfigDoc.get("basic_auth").required().asDocNode());
                } catch (ConfigValidationException e) {
                    validationErrors.add("tls", e);
                }
                validationErrors.throwExceptionForPresentErrors();
                if (this.hostname == null) {
                    this.hostname = hostname;
                }
                if (!customPort) {
                    this.port = port;
                }
            }

            private void validate() throws ConfigValidationException {
                ValidationErrors validationErrors = new ValidationErrors();
                if (hostname == null) {
                    validationErrors.add(new MissingAttribute("--host"));
                }
                if (username == null) {
                    validationErrors.add(new MissingAttribute("--user"));
                }
                if (password == null) {
                    validationErrors.add(new MissingAttribute("--password"));
                }
                validationErrors.throwExceptionForPresentErrors();
            }

            public Profile build(String mergeProfileName) throws ToolException {
                if (profileExists(confDir, mergeProfileName)) {
                    File profileFile = new File(confDir, "profile_" + mergeProfileName + ".yml");
                    try {
                        DocNode profileDoc = DocNode.wrap(DocReader.yaml().readObject(profileFile));
                        read(profileDoc);
                    } catch (DocumentParseException | IOException | UnexpectedDocumentStructureException e) {
                        throw new ToolException("Error while reading '" + profileFile + "'", e);
                    } catch (ConfigValidationException e) {
                        throw new ToolException("Invalid profile '" + profileFile + "'", e);
                    }
                } else {
                    basicAuthConfig = new BasicAuthConfig(username, password);
                    try {
                        if (caCert != null) {
                            tlsConfig = new TLSConfig.Builder().trust(caCert).verifyHostnames(!insecure).build();
                        } else {
                            tlsConfig = new TLSConfig.Builder().verifyHostnames(!insecure).build();
                        }
                    } catch (ConfigValidationException e) {
                        throw new ToolException("Error while creating tls configuration", e);
                    }
                }
                try {
                    validate();
                } catch (ConfigValidationException e) {
                    throw new ToolException("Invalid profile settings", e);
                }
                return new Profile(hostname, port, tlsConfig, basicAuthConfig);
            }
        }
    }

    public static class ToolException extends Exception {
        @Serial
        private final static long serialVersionUID = -963587291045738489L;

        public static ToolException from(Exception e) {
            if (e instanceof ToolException toolException) {
                return toolException;
            } else if (e instanceof SearchGuardRestClient.FailedConnectionException failedConnectionException) {
                return from(failedConnectionException);
            } else if (e instanceof SearchGuardRestClient.InvalidResponseException || e instanceof SearchGuardRestClient.ApiException) {
                return new ToolException("Invalid response from server: " + e.getMessage(), e);
            } else if (e instanceof SearchGuardRestClient.ServiceUnavailableException) {
                return new ToolException("Server is unavailable: " + e.getMessage(), e);
            } else if (e instanceof SearchGuardRestClient.UnauthorizedException) {
                return new ToolException("Server rejected request as unauthorized. Please check user permissions.", e);
            } else {
                return new ToolException(e);
            }
        }

        private static ToolException from(SearchGuardRestClient.FailedConnectionException e) {
            String message = e.getMessage();
            if (e.getCause() instanceof SSLHandshakeException
                    && e.getMessage().contains("unable to find valid certification path to requested target")) {
                message = "Could not validate server certificate using current CA settings. "
                        + "Please verify that you are using the correct CA certificates. "
                        + "You can specify custom CA certificates using the --ca-cert option.";
            } else if (e.getCause() instanceof SSLException && e.getCause().getCause() instanceof SocketException) {
                message = "Connection failed: " + e.getCause().getCause().getMessage();
            }
            return new ToolException(message, e);
        }

        public ToolException(String message) {
            super(message);
        }

        public ToolException(String message, Throwable cause) {
            super(message, cause);
        }

        public ToolException(Throwable cause) {
            super(cause);
        }

        public ToolException(String message, ConfigValidationException e) {
            this(message + "\n" + e.getValidationErrors(), (Throwable) e);
        }
    }
}
