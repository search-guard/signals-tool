package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "deactivate-watch", description = "Deactivates a watch")
public class DeactivateWatch extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        try {
            client.deleteWatchActive(tenant, watchName);
        } catch (SearchGuardRestClient.ApiException e) {
            //Workaround for error message in case the watch is not found. The error message provided by the Api is not at all helpful
            if (e.getHttpResponse().getStatusLine().getStatusCode() == 404) {
                throw new SearchGuardRestClient.ApiException("Not found", e.getHttpResponse());
            }
            throw e;
        }
        System.out.println("Deactivated watch '" + watchName + "' on tenant '" + tenant + "'");
    }
}
