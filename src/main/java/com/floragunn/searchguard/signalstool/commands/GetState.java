package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.GetWatchStateResponse;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "get-state", description = "Gets a state from a watch")
public class GetState extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        GetWatchStateResponse response = client.getWatchState(tenant, watchName);
        printInfo("Name", watchName, 0);
        printInfo("Tenant", tenant, 0);
        printInfo("Severity",
                response.getLastExecution() != null && response.getLastExecution().severity() != null
                        ? response.getLastExecution().severity().level().getName()
                        : "not configured",
                0);
        printInfo("Status",
                response.getLastStatus() != null && response.getLastStatus().code() != null ? response.getLastStatus().code().name().replace('_', ' ')
                        : "unknown",
                0);
        if (response.getLastStatus() != null && response.getLastStatus().detail() != null) {
            printInfo("Status detail", response.getLastStatus().detail(), 0);
        }
        if (response.getActions().isEmpty()) {
            printInfo("Actions", "none", 0);
        } else {
            printInfo("Actions", "", 0);
            for (GetWatchStateResponse.Action action : response.getActions()) {
                printInfo(action.name(), "", 4);
                printInfo("Last triggered", action.lastTriggered() != null ? action.lastTriggered() : "unknown", 8);
                printInfo("Acknowledged", Boolean.toString(action.acknowledged()), 8);
                printInfo("Status",
                        action.lastStatus() != null && action.lastStatus().code() != null ? action.lastStatus().code().name().replace('_', ' ')
                                : "unknown",
                        8);
                if (action.lastStatus() != null && action.lastStatus().detail() != null) {
                    printInfo("Status detail", action.lastStatus().detail(), 8);
                }
            }
        }
    }

    private static void printInfo(String key, String value, int indentation) {
        System.out.print(" ".repeat(indentation));
        System.out.printf("%-15s %s%n", key + ":", value);
    }
}
