package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.*;
import com.floragunn.searchguard.signalstool.SignalsTool;
import picocli.CommandLine.Option;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class AbstractWatchCommand extends AbstractConnectingCommand {
    @Option(names = { "--tenant" }, defaultValue = "_main", description = "Tenant to be used by this command")
    String tenant;

    public static class InputGroup {
        @Option(names = { "--input" }, required = true, description = "Path to input file")
        File file;

        @Option(names = { "--json" }, required = true, description = "JSON string input")
        String json;

        public String getAsJsonString() throws SignalsTool.ToolException {
            try {
                Object res = json != null ? DocReader.json().read(json) : DocReader.json().read(file);
                return DocWriter.json().writeAsString(res);
            } catch (DocumentParseException e) {
                throw SignalsTool.ToolException.from(e);
            } catch (IOException e) {
                throw new SignalsTool.ToolException("Error while reading input from " + file + ": " + e.getMessage(), e);
            }
        }

        public DocNode getAsDocNode() throws SignalsTool.ToolException {
            try {
                return DocNode.wrap(json != null ? DocReader.json().readObject(json) : DocReader.json().readObject(file));
            } catch (DocumentParseException | UnexpectedDocumentStructureException e) {
                throw new SignalsTool.ToolException("Input file '" + file + "' does not contain valid json: " + e.getMessage(), e);
            } catch (FileNotFoundException e) {
                throw new SignalsTool.ToolException("Input file '" + file + "' does not exist: " + e.getMessage(), e);
            } catch (IOException e) {
                throw new SignalsTool.ToolException("Error while reading input from " + file + ": " + e.getMessage(), e);
            }
        }
    }

    public static class OutputGroup {
        @Option(names = { "--output" }, description = "Path to output file or directory")
        File file;

        public void handle(Object output, String fileName) throws SignalsTool.ToolException {
            File file = this.file;
            String suffix = ".json";
            if (file.isDirectory()) {
                file = new File(file, fileName + suffix);
            }
            try {
                DocWriter.json().write(file, output);
            } catch (IOException e) {
                throw new SignalsTool.ToolException("Error while writing output to " + file + ": " + e.getMessage(), e);
            }
        }
    }
}
