package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.SignalsTool;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.AuthInfoResponse;
import picocli.CommandLine.Command;

@Command(name = "connect", description = "Tries to connect to a cluster and persists this connection for subsequent commands")
public class Connect extends AbstractConnectingCommand {

    @Override
    public Integer call() {
        try {
            SignalsTool.Profile.deleteSelectedProfileName(confDir);

            SearchGuardRestClient client = getClient();
            AuthInfoResponse response = client.getAuthInfo();
            System.out.println("Successfully connected to cluster '" + response.getClusterName() + "' with user '" + response.getUserName() + "'");

            if (containsOverrideOptions()) {
                if (profileName == null) {
                    profileName = getProfile().hostname() + "_" + response.getUserName();
                }
                if (SignalsTool.Profile.profileExists(confDir, profileName)) {
                    System.out.println("Overwriting profile " + profileName);
                } else {
                    System.out.println("Saving new profile " + profileName + " to " + confDir.getAbsolutePath());
                }
                getProfile().write(confDir, profileName);
            }
            SignalsTool.Profile.writeSelectedProfileName(confDir, profileName);
        } catch (Exception e) {
            System.err.println(SignalsTool.ToolException.from(e).getMessage());
            if (verbose) {
                e.printStackTrace();
            }
            return 1;
        }
        return 0;
    }
}
