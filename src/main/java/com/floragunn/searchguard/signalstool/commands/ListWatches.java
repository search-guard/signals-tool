package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.GetWatchResponse;
import com.floragunn.searchguard.signalstool.client.api.ListWatchesResponse;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "list-watches", description = "Lists selected watches")
public class ListWatches extends AbstractWatchCommand {
    @Option(names = { "--size" }, defaultValue = "10", description = "Maximum amount of watches to show")
    int size;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        ListWatchesResponse response = client.postWatchSearch(tenant, DocNode.EMPTY.toJsonString(), size);
        System.out.printf("%-30s %-30s%n", "name", "active");
        System.out.println("-".repeat(60));
        for (GetWatchResponse watchResponse : response.getWatchResponses()) {
            System.out.printf("%-30s %-30s%n", watchResponse.getWatchName(), watchResponse.isActive());
        }
        if (response.getTotalHits() > response.getWatchResponses().size()) {
            System.out.printf("Showing %d of %d watches. Use --size to show more%n", response.getWatchResponses().size(), response.getTotalHits());
        }
    }
}
