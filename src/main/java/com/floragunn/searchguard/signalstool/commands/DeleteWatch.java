package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.DeleteWatchResponse;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Command;

@Command(name = "delete-watch", description = "Deletes a watch from the cluster")
public class DeleteWatch extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        DeleteWatchResponse response = client.deleteWatch(tenant, watchName);
        if ("deleted".equals(response.getResult())) {
            System.out.println("Successfully deleted watch '" + watchName + "' on tenant '" + tenant + "'");
        }
    }
}
