package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.GetWatchStateResponse;
import com.floragunn.searchguard.signalstool.client.api.ListWatchStatesResponse;
import com.google.common.base.Strings;
import picocli.CommandLine.Command;
import picocli.CommandLine.Help.Ansi;
import picocli.CommandLine.Option;

import java.util.Iterator;
import java.util.List;

@Command(name = "list-states", description = "Lists states of selected watches")
public class ListStates extends AbstractWatchCommand {
    @Option(names = { "--size" }, defaultValue = "10", description = "Maximum amount of states to show")
    int size;

    public static final String WARNING_COLOR = "fg(226)";
    public static final String CRITICAL_COLOR = "fg(196)";
    public static final String ERROR_COLOR = "fg(214)";

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        ListWatchStatesResponse response = client.postWatchSearchState(tenant, DocNode.EMPTY.toJsonString(), size);
        System.out.printf("%-29s %-29s %-30s%n", "status", "name", "actions");
        System.out.println("-".repeat(90));
        List<GetWatchStateResponse> stateResponses = response.getWatchStateResponses();
        for (GetWatchStateResponse stateResponse : stateResponses) {
            System.out.print(abbreviate(watchStateToText(stateResponse), 29));
            System.out.print(" ");
            System.out.print(abbreviate(Ansi.AUTO.text(stateResponse.getName()), 29));
            System.out.print(" ");
            if (stateResponse.getLastStatus() != null && stateResponse.getLastStatus().code() != null
                    && stateResponse.getLastStatus().code() == GetWatchStateResponse.LastStatus.Code.EXECUTION_FAILED) {
                System.out.print(" ".repeat(30));
                System.out.println();
            } else {
                Iterator<GetWatchStateResponse.Action> actions = stateResponse.getActions().iterator();
                GetWatchStateResponse.Action action = actions.hasNext() ? actions.next() : null;
                System.out.print(action != null ? abbreviate(actionToText(action), 30) : " ".repeat(30));
                System.out.println();
                while (actions.hasNext()) {
                    action = actions.next();
                    System.out.print(" ".repeat(60));
                    System.out.print(abbreviate(actionToText(action), 30));
                    System.out.println();
                }
            }
        }
        if (response.getTotalHits() > stateResponses.size()) {
            System.out.printf("Showing %d of %d states. Use --size to show more%n", stateResponses.size(), response.getTotalHits());
        }
    }

    private static Ansi.Text watchStateToText(GetWatchStateResponse watchState) {
        if (watchState.getLastStatus() != null && watchState.getLastStatus().code() != null) {
            switch (watchState.getLastStatus().code()) {
            case EXECUTION_FAILED -> {
                return Ansi.AUTO.text("@|fg(244) Failed|@");
            }
            case NO_ACTION, ACTION_THROTTLED -> {
                return Ansi.AUTO.text("@|green Active|@");
            }
            case ACTION_EXECUTED, ACTION_FAILED, SIMULATED_ACTION_EXECUTED, ACKED -> {
                StringBuilder formatBuilder = new StringBuilder();
                String statusName;
                boolean unacknowledged = !watchState.getActions().isEmpty()
                        && watchState.getActions().stream().noneMatch(GetWatchStateResponse.Action::acknowledged);
                if (unacknowledged) {
                    formatBuilder.append("bold");
                }
                if (watchState.getLastExecution() != null && watchState.getLastExecution().severity() != null
                        && watchState.getLastExecution().severity().level() != null) {
                    String color = switch (watchState.getLastExecution().severity().level()) {
                    case WARNING -> WARNING_COLOR;
                    case CRITICAL -> CRITICAL_COLOR;
                    case ERROR -> ERROR_COLOR;
                    default -> "";
                    };
                    if (!formatBuilder.isEmpty() && !Strings.isNullOrEmpty(color)) {
                        formatBuilder.append(",");
                    }
                    formatBuilder.append(color);
                    statusName = watchState.getLastExecution().severity().level().getName().toLowerCase();
                    statusName = statusName.substring(0, 1).toUpperCase() + statusName.substring(1);
                } else {
                    if (!formatBuilder.isEmpty()) {
                        formatBuilder.append(",yellow");
                    } else {
                        formatBuilder.append("yellow");
                    }
                    statusName = "Action";
                }
                return Ansi.AUTO.text("@|" + formatBuilder + (formatBuilder.isEmpty() ? "" : " ") + statusName + "|@");
            }
            }
        }
        return Ansi.AUTO.text("Unknown");
    }

    private static String abbreviate(Ansi.Text text, int len) {
        if (text.getCJKAdjustedLength() <= len) {
            return text + " ".repeat(len - text.getCJKAdjustedLength());
        }
        return text.substring(0, len - 3) + "...";
    }

    private static Ansi.Text actionToText(GetWatchStateResponse.Action action) {
        if (!action.acknowledged()) {
            return Ansi.AUTO.text("@|bold " + action.name() + "|@");
        }
        return Ansi.AUTO.text(action.name());
    }
}
