package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.DocWriter;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.GetSettingsResponse;
import com.floragunn.searchguard.signalstool.commands.AbstractWatchCommand.OutputGroup;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "get-setting", description = "Gets Signals settings from the cluster")
public class GetSettings extends AbstractConnectingCommand {
    @Parameters(arity = "0..1", description = { "Key of the setting. If no key is provided all settings are requested" })
    String key;

    @ArgGroup(multiplicity = "0..1", exclusive = true)
    OutputGroup output;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        GetSettingsResponse response = client.getSettings(key);
        if (output != null) {
            output.handle(response.getSettings(), "signals_settings_" + getProfile().hostname());
        }
        System.out.println(DocWriter.json().pretty().writeAsString(response.getSettings()));
    }
}
