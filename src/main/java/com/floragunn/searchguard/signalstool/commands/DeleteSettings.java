package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "delete-setting", description = "Deletes a Signals setting on the cluster")
public class DeleteSettings extends AbstractConnectingCommand {
    @Parameters(arity = "1", description = { "Key of the setting" })
    String key;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        client.deleteSetting(key);
    }
}
