package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.commands.AbstractWatchCommand.InputGroup;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Command;
import picocli.CommandLine.ArgGroup;

@Command(name = "update-setting", description = "Updates a Signals setting on the cluster")
public class UpdateSettings extends AbstractConnectingCommand {
    @Parameters(arity = "1", description = { "Key of the setting" })
    String key;

    @ArgGroup(multiplicity = "1", exclusive = true)
    InputGroup input;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        client.putSettings(key, input.getAsJsonString());
    }
}
