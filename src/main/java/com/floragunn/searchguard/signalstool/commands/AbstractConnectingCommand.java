package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.SignalsTool;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.Spec;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static com.floragunn.searchguard.signalstool.SignalsTool.Profile.readSelectedProfileName;

public abstract class AbstractConnectingCommand implements Callable<Integer> {
    @Spec
    CommandSpec spec;
    @Option(names = { "-h", "--host" }, description = "Hostname of the node to connect to")
    String hostname;

    @Option(names = { "-p", "--port" }, description = "REST port to connect to. Default: 9200", defaultValue = "9200")
    Integer port;

    @Option(names = { "-u", "--user" }, description = "Username")
    String username;

    @Option(names = { "--password" }, description = "Password", arity = "0..1", interactive = true)
    String password;

    @Option(names = { "--ca-cert" }, description = "Trusted Certificates")
    File caCert;

    @Option(names = { "--insecure" }, description = "Do not verify the hostname when connecting to the cluster")
    boolean insecure;

    @Option(names = "--profile", description = "Profile to be used by this command", defaultValue = Option.NULL_VALUE)
    String profileName;

    @Option(names = { "--debug" }, defaultValue = "false", description = "Print debug information")
    boolean debug;

    @Option(names = { "--verbose", "-v" }, defaultValue = "false", description = "Print more information")
    boolean verbose;

    @Option(names = "--signals-tool-config-dir", description = "The directory where this tool reads and writes profiles", defaultValue = "${user.home}/.searchguard")
    public void setConfDir(File confDir) {
        if (confDir.isFile()) {
            throw new ParameterException(spec.commandLine(), "The path specified by --signals-tool-config-dir must be a directory");
        }
        this.confDir = confDir;
    }

    File confDir;

    private SignalsTool.Profile profile;

    @Override
    public Integer call() {
        try (SearchGuardRestClient client = getClient().debug(debug).verbose(verbose)) {
            runCommandAction(client);
        } catch (Exception e) {
            System.err.println(SignalsTool.ToolException.from(e).getMessage());
            if (verbose) {
                e.printStackTrace();
            }
            return 1;
        }
        return 0;
    }

    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        throw new IllegalStateException("Not implemented");
    }

    protected SearchGuardRestClient getClient() throws SignalsTool.ToolException {
        try {
            return new SearchGuardRestClient(getProfile().toHttpHost(), getProfile().tlsConfig(), getProfile().basicAuthConfig().toCredentials());
        } catch (Exception e) {
            throw SignalsTool.ToolException.from(e);
        }
    }

    protected SignalsTool.Profile getProfile() throws SignalsTool.ToolException {
        if (profile == null) {
            if (profileName == null) {
                profileName = readSelectedProfileName(confDir);
            }
            SignalsTool.Profile.Builder builder = new SignalsTool.Profile.Builder(confDir).hostname(hostname).port(port)
                    .customPort(containsOption("--port") || containsOption("-p")).username(username).password(password).caCert(caCert)
                    .insecure(insecure).customInsecure(containsOption("--insecure"));
            profile = builder.build(profileName);
        }
        return profile;
    }

    protected boolean containsOption(String option) {
        return spec.commandLine().getParseResult().hasMatchedOption(option);
    }

    protected boolean containsOverrideOptions() {
        for (CommandLine.Model.OptionSpec matchedOption : spec.commandLine().getParseResult().matchedOptions()) {
            List<String> names = Arrays.asList(matchedOption.names());
            if (names.contains("-h") || names.contains("--host") || names.contains("-p") || names.contains("--port") || names.contains("-u")
                    || names.contains("--user") || names.contains("--password") || names.contains("--ca-cert") || names.contains("--insecure")) {
                return true;
            }
        }
        return false;
    }
}
