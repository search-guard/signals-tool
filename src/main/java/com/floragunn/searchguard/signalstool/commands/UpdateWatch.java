package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.PutWatchResponse;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "update-watch", description = "Updates a watch on the cluster")
public class UpdateWatch extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @Option(names = { "--inactive" }, description = "Marks the watch as inactive")
    boolean inactive;

    @ArgGroup(multiplicity = "1", exclusive = true)
    InputGroup input;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        DocNode watch = input.getAsDocNode().with("active", !inactive);
        PutWatchResponse response = client.putWatch(tenant, watchName, watch.toJsonString());
        if ("created".equals(response.getResult())) {
            System.out.println("Successfully created watch '" + watchName + "'");
        } else if ("updated".equals(response.getResult())) {
            System.out.println("Successfully updated watch '" + watchName + "'");
        }
    }
}
