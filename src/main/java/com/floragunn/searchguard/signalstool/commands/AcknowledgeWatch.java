package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "ack-watch", description = "Acknowledges a watch")
public class AcknowledgeWatch extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @Option(names = { "--action" }, description = "Acknowledge only a specific action of the watch")
    String action;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        client.putWatchAcknowledge(tenant, watchName, action);
    }
}
