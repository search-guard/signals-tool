package com.floragunn.searchguard.signalstool.commands;

import com.floragunn.codova.documents.DocWriter;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import com.floragunn.searchguard.signalstool.client.api.GetWatchResponse;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

@Command(name = "get-watch", description = "Gets a watch from the cluster")
public class GetWatch extends AbstractWatchCommand {
    @Parameters(arity = "1", description = { "Name of the watch" })
    String watchName;

    @ArgGroup(multiplicity = "0..1", exclusive = true)
    OutputGroup output;

    @Override
    public void runCommandAction(SearchGuardRestClient client) throws Exception {
        GetWatchResponse response = client.getWatch(tenant, watchName);
        if (output != null) {
            output.handle(response.getWatch(), response.getWatchName());
        } else {
            System.out.println(DocWriter.json().pretty().writeAsString(response.getWatch()));
        }
    }
}
