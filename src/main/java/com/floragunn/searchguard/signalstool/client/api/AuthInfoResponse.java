package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

public class AuthInfoResponse extends BasicResponse {
    private final String username;
    private final String clusterName;

    public AuthInfoResponse(SearchGuardRestClient.Response response) throws SearchGuardRestClient.InvalidResponseException {
        super(response);
        username = content.getAsString("user_name");
        clusterName = content.getAsString("cluster_name");
    }

    public String getUserName() {
        return username;
    }

    public String getClusterName() {
        return clusterName;
    }
}
