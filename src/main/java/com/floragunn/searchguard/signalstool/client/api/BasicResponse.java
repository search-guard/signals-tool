package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

public class BasicResponse {
    protected final DocNode content;
    protected final String contentType;
    protected final String eTag;

    public BasicResponse(SearchGuardRestClient.Response response) throws SearchGuardRestClient.InvalidResponseException {
        content = response.asDocNode();
        contentType = response.getContentType();
        eTag = response.getETag();
    }

    public BasicResponse(DocNode content, String contentType, String eTag) {
        this.content = content;
        this.contentType = contentType;
        this.eTag = eTag;
    }
}
