package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import org.apache.http.entity.ContentType;

public class GetWatchResponse extends BasicResponse {
    private final DocNode watch;
    private final String id;
    private final boolean active;

    public GetWatchResponse(SearchGuardRestClient.Response response) throws SearchGuardRestClient.InvalidResponseException {
        super(response);
        id = content.getAsString("_id");
        watch = content.getAsNode("_source").without("_meta").without("_tenant");
        try {
            active = watch.hasNonNull("active") && watch.getBoolean("active");
        } catch (ConfigValidationException e) {
            throw SearchGuardRestClient.InvalidResponseException.from(e);
        }
    }

    public GetWatchResponse(DocNode watchContent) throws SearchGuardRestClient.InvalidResponseException {
        super(watchContent, ContentType.APPLICATION_JSON.getMimeType(), null);
        id = watchContent.getAsString("_id");
        watch = watchContent.getAsNode("_source").without("_meta").without("_tenant");
        try {
            active = watch.hasNonNull("active") && watch.getBoolean("active");
        } catch (ConfigValidationException e) {
            throw SearchGuardRestClient.InvalidResponseException.from(e);
        }
    }

    public DocNode getWatch() {
        return watch;
    }

    public String getWatchId() {
        return id;
    }

    public String getWatchName() {
        return id.substring(id.indexOf('/') + 1);
    }

    public boolean isActive() {
        return active;
    }
}
