package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

public class PutWatchResponse extends BasicResponse {
    private final String id;
    private final Integer version;
    private final String result;

    public PutWatchResponse(SearchGuardRestClient.Response response) throws SearchGuardRestClient.InvalidResponseException {
        super(response);
        id = content.getAsString("_id");
        version = (Integer) content.get("_version");
        result = content.getAsString("result");
    }

    public String getWatchId() {
        return id;
    }

    public Integer getVersion() {
        return version;
    }

    public String getResult() {
        return result;
    }
}
