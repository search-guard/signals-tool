package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

import java.util.List;

public class ListWatchesResponse extends BasicResponse {
    private final List<GetWatchResponse> watchResponses;
    private final long totalHits;

    public ListWatchesResponse(SearchGuardRestClient.Response response)
            throws SearchGuardRestClient.InvalidResponseException, ConfigValidationException {
        super(response);
        ValidationErrors errors = new ValidationErrors();
        ValidatingDocNode hits = new ValidatingDocNode(content.getAsNode("hits"), errors);
        totalHits = hits.get("total", "value").asLong();
        watchResponses = hits.get("hits").asList(GetWatchResponse::new);
        errors.throwExceptionForPresentErrors();
    }

    public List<GetWatchResponse> getWatchResponses() {
        return watchResponses;
    }

    public long getTotalHits() {
        return totalHits;
    }
}
