package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

public class GetSettingsResponse {
    Object settings;

    public GetSettingsResponse(SearchGuardRestClient.Response response) throws SearchGuardRestClient.InvalidResponseException {
        settings = response.asObject();
    }

    public Object getSettings() {
        return settings;
    }
}
