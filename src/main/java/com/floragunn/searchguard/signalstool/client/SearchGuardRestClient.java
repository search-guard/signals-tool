package com.floragunn.searchguard.signalstool.client;

import com.floragunn.codova.config.net.TLSConfig;
import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.documents.DocReader;
import com.floragunn.codova.documents.DocumentParseException;
import com.floragunn.codova.documents.Format;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.searchguard.signalstool.client.api.*;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serial;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SearchGuardRestClient implements AutoCloseable {
    private static final Logger log = Logger.getLogger(SearchGuardRestClient.class.getName());
    private final CloseableHttpClient client;
    private final HttpHost host;

    private boolean debug = false;
    private boolean verbose = false;

    public SearchGuardRestClient(HttpHost host, TLSConfig tlsConfig, UsernamePasswordCredentials credentials) {
        this.host = host;
        final BasicCredentialsProvider provider = new BasicCredentialsProvider();
        provider.setCredentials(new AuthScope(host), credentials);
        client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).setSSLSocketFactory(tlsConfig.toSSLConnectionSocketFactory())
                .build();
    }

    public SearchGuardRestClient debug(boolean debug) {
        this.debug = debug;
        return this;
    }

    public SearchGuardRestClient verbose(boolean verbose) {
        this.verbose = verbose;
        return this;
    }

    public AuthInfoResponse getAuthInfo()
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return get("/_searchguard/authinfo").parse(AuthInfoResponse::new);
    }

    public GetWatchResponse getWatch(String tenant, String name)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return get("/_signals/watch/" + tenant + "/" + name).parse(GetWatchResponse::new);
    }

    public PutWatchResponse putWatch(String tenant, String name, String jsonContent)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return put("/_signals/watch/" + tenant + "/" + name, jsonContent, ContentType.APPLICATION_JSON).parse(PutWatchResponse::new);
    }

    public DeleteWatchResponse deleteWatch(String tenant, String name)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return delete("/_signals/watch/" + tenant + "/" + name).parse(DeleteWatchResponse::new);
    }

    public ListWatchesResponse postWatchSearch(String tenant, String body, int size)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return post("/_signals/watch/" + tenant + "/_search?size=" + size, body, ContentType.APPLICATION_JSON).parse(ListWatchesResponse::new);
    }

    public Response putWatchActive(String tenant, String name)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return put("/_signals/watch/" + tenant + "/" + name + "/_active", null, ContentType.APPLICATION_JSON).checkStatus();
    }

    public Response deleteWatchActive(String tenant, String name)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return delete("/_signals/watch/" + tenant + "/" + name + "/_active").checkStatus();
    }

    public Response putWatchAcknowledge(String tenant, String name, String action)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        String endpoint = "/_signals/watch/" + tenant + "/" + name + "/" + "_ack" + (action != null ? "/" + action : "");
        return put(endpoint, null, ContentType.APPLICATION_JSON).checkStatus();
    }

    public Response deleteWatchAcknowledge(String tenant, String name, String action)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        String endpoint = "/_signals/watch/" + tenant + "/" + name + "/" + "_ack" + (action != null ? "/" + action : "");
        return delete(endpoint).checkStatus();
    }

    public GetWatchStateResponse getWatchState(String tenant, String name)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return get("/_signals/watch/" + tenant + "/" + name + "/_state").parse(response -> new GetWatchStateResponse(name, response));
    }

    public ListWatchStatesResponse postWatchSearchState(String tenant, String body, int size)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return post("/_signals/watch/" + tenant + "/_search/_state?size=" + size, body, ContentType.APPLICATION_JSON)
                .parse(ListWatchStatesResponse::new);
    }

    public GetSettingsResponse getSettings(String key)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return get("/_signals/settings" + (Strings.isNullOrEmpty(key) ? "" : "/" + key)).parse(GetSettingsResponse::new);
    }

    public Response putSettings(String key, String body)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return put("/_signals/settings/" + key, body, ContentType.APPLICATION_JSON).checkStatus();
    }

    public Response deleteSetting(String key)
            throws FailedConnectionException, InvalidResponseException, UnauthorizedException, ServiceUnavailableException, ApiException {
        return delete("/_signals/settings/" + key).checkStatus();
    }

    private Response get(String path) throws FailedConnectionException, InvalidResponseException {
        try {
            return new Response(client.execute(host, new HttpGet(path)));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    private Response post(String path, String body, ContentType contentType) throws FailedConnectionException, InvalidResponseException {
        HttpPost post = new HttpPost(path);
        post.setEntity(new StringEntity(body, contentType));
        try {
            return new Response(client.execute(host, post));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    private Response post(String path) throws FailedConnectionException, InvalidResponseException {
        try {
            return new Response(client.execute(host, new HttpPost(path)));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    private Response put(String path, String body, ContentType contentType, Header... headers)
            throws FailedConnectionException, InvalidResponseException {
        HttpPut put = new HttpPut(path);
        put.setHeaders(headers);
        if (body != null) {
            put.setEntity(new StringEntity(body, contentType));
        }
        try {
            return new Response(client.execute(host, put));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    private Response patch(String path, String body, ContentType contentType, Header... headers)
            throws FailedConnectionException, InvalidResponseException {
        HttpPatch patch = new HttpPatch(path);
        patch.setHeaders(headers);
        patch.setEntity(new StringEntity(body, contentType));
        try {
            return new Response(client.execute(host, patch));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    private Response delete(String path) throws FailedConnectionException, InvalidResponseException {
        try {
            return new Response(client.execute(host, new HttpDelete(path)));
        } catch (IOException e) {
            throw FailedConnectionException.from(e);
        }
    }

    @Override
    public void close() {
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ApiException extends Exception {
        @Serial
        private static final long serialVersionUID = -1526383197236858960L;

        private final HttpResponse httpResponse;

        private final ValidationErrors validationErrors;

        public ApiException(String message, HttpResponse response, Throwable cause) {
            super(message, cause);
            httpResponse = response;
            validationErrors = null;
        }

        public ApiException(String message, HttpResponse response) {
            this(message, response, (ValidationErrors) null);
        }

        private ApiException(String message, HttpResponse response, ValidationErrors validationErrors) {
            super(message);
            httpResponse = response;
            this.validationErrors = validationErrors;
        }

        public static ApiException createBadRequestJsonException(HttpResponse httpResponse, String body) throws InvalidResponseException {
            DocNode responseDoc;
            try {
                responseDoc = DocNode.wrap(DocReader.json().read(body));
            } catch (DocumentParseException e) {
                throw InvalidResponseException.from(e);
            }
            String message = null;
            ValidationErrors validationErrors = null;
            if (responseDoc.get("error") instanceof String) {
                message = responseDoc.getAsString("error");
            } else if (responseDoc.get("error") instanceof Map) {
                DocNode errorDoc = responseDoc.getAsNode("error");
                if (errorDoc.get("reason") instanceof String) {
                    message = errorDoc.getAsString("reason").startsWith("Invalid index name [_searchguard]") ? "Invalid REST endpoint"
                            : errorDoc.getAsString("reason");
                }
                if (errorDoc.get("message") instanceof String) {
                    message = errorDoc.getAsString("message");
                }
                if (errorDoc.get("details") instanceof Map) {
                    try {
                        validationErrors = ValidationErrors.parse(errorDoc.getAsNode("details"));
                    } catch (Exception e) {
                        log.log(Level.WARNING, "Error while parsing validation errors in response", e);
                    }
                }
            }
            if (responseDoc.get("detail") instanceof Map) {
                try {
                    validationErrors = ValidationErrors.parse(responseDoc.getAsNode("detail"));
                } catch (Exception e) {
                    log.log(Level.WARNING, "Error while parsing validation errors in response", e);
                }
            }
            if (message == null) {
                message = httpResponse.getStatusLine().toString();
            }
            return new ApiException(message, httpResponse, validationErrors);
        }

        public ValidationErrors getValidationErrors() {
            return validationErrors;
        }

        public HttpResponse getHttpResponse() {
            return httpResponse;
        }
    }

    public static class FailedConnectionException extends Exception {
        @Serial
        private final static long serialVersionUID = -730952626233956393L;

        static FailedConnectionException from(IOException ioException) {
            if (ioException instanceof SSLHandshakeException) {
                return new FailedConnectionException((SSLHandshakeException) ioException);
            } else if (ioException instanceof ConnectException) {
                return new FailedConnectionException((ConnectException) ioException);
            } else {
                return new FailedConnectionException(ioException);
            }
        }

        public FailedConnectionException(String message) {
            super(message);
        }

        public FailedConnectionException(String message, Throwable cause) {
            super(message, cause);
        }

        public FailedConnectionException(Throwable cause) {
            super(cause);
        }

        public FailedConnectionException(ConnectException e) {
            this(e.getMessage(), e);
        }

        public FailedConnectionException(SSLHandshakeException e) {
            this("TLS handshake failed while creating new connection: " + e.getMessage(), e);
        }
    }

    public static class InvalidResponseException extends Exception {
        @Serial
        private final static long serialVersionUID = 4323718532754371670L;

        public static InvalidResponseException from(DocumentParseException e) {
            return new InvalidResponseException("Response contains invalid JSON: " + e.getMessage(), e);
        }

        public static InvalidResponseException from(ConfigValidationException e) {
            return new InvalidResponseException("Response contains invalid JSON: " + e.getMessage(), e);
        }

        public InvalidResponseException(String message) {
            super(message);
        }

        public InvalidResponseException(String message, Throwable cause) {
            super(message, cause);
        }

        public InvalidResponseException(Throwable cause) {
            super(cause);
        }
    }

    public static class ServiceUnavailableException extends Exception {
        @Serial
        private static final long serialVersionUID = 5379441862642349161L;

        public ServiceUnavailableException(String message) {
            super(message);
        }
    }

    public static class UnauthorizedException extends Exception {
        @Serial
        private static final long serialVersionUID = -3419077395036123237L;

        public UnauthorizedException(String message) {
            super(message);
        }
    }

    public static class PreconditionFailedException extends ApiException {

        @Serial
        private static final long serialVersionUID = -5749047361613933983L;

        public PreconditionFailedException(String message, HttpResponse response) {
            super(message, response);
        }

        public PreconditionFailedException(String message, HttpResponse response, Throwable cause) {
            super(message, response, cause);
        }
    }

    public class Response {
        private final HttpResponse httpResponse;
        private final String eTag;
        private final String contentType;
        private final String body;

        protected Response(HttpResponse httpResponse) throws InvalidResponseException {
            this.httpResponse = httpResponse;
            eTag = httpResponse.containsHeader("ETag") ? httpResponse.getFirstHeader("ETag").getValue() : null;
            if (httpResponse.getEntity() != null) {
                HttpEntity entity = httpResponse.getEntity();
                contentType = entity.getContentType() != null ? ContentType.parse(entity.getContentType().getValue()).getMimeType().toLowerCase()
                        : null;
                Charset charset = entity.getContentEncoding() != null ? Charset.forName(entity.getContentEncoding().getValue()) : Charsets.UTF_8;
                try {
                    if (debug || verbose) {
                        System.out.println("-".repeat(60));
                        System.out.println(this.httpResponse.getStatusLine());
                        System.out.println("Content-Type: " + this.contentType);
                    }
                    body = CharStreams.toString(new InputStreamReader(entity.getContent(), charset));
                    if (!Strings.isNullOrEmpty(body) && (debug || verbose)) {
                        String bodyOutput = body;
                        if (debug && bodyOutput.length() > 240) {
                            bodyOutput = bodyOutput.substring(0, 240) + "...";
                        }
                        System.out.println(bodyOutput);
                    }
                } catch (IllegalCharsetNameException | UnsupportedCharsetException | IOException e) {
                    throw new InvalidResponseException(e);
                } finally {
                    if (debug || verbose) {
                        System.out.println("-".repeat(60));
                    }
                }
            } else {
                contentType = null;
                body = null;
            }
        }

        public String getETag() {
            return eTag;
        }

        public String getContentType() {
            return contentType;
        }

        public DocNode asDocNode() throws InvalidResponseException {
            if (Strings.isNullOrEmpty(body)) {
                return DocNode.EMPTY;
            }
            Format docType = Format.peekByContentType(contentType);
            try {
                return docType != null ? DocNode.wrap(DocReader.format(docType).read(body)) : DocNode.wrap(body);
            } catch (DocumentParseException e) {
                throw new InvalidResponseException(e);
            }
        }

        public Object asObject() throws InvalidResponseException {
            if (Strings.isNullOrEmpty(body)) {
                return null;
            }
            Format docType = Format.peekByContentType(contentType);
            try {
                return docType != null ? DocReader.format(docType).read(body) : body;
            } catch (DocumentParseException e) {
                throw new InvalidResponseException(e);
            }
        }

        public <T> T parse(Parser<T> parser) throws InvalidResponseException, ServiceUnavailableException, UnauthorizedException, ApiException {
            checkStatus();
            try {
                return parser.apply(this);
            } catch (ConfigValidationException e) {
                throw InvalidResponseException.from(e);
            }
        }

        public Response checkStatus() throws InvalidResponseException, ServiceUnavailableException, UnauthorizedException, ApiException {
            switch (httpResponse.getStatusLine().getStatusCode()) {
            case 503 -> throw new ServiceUnavailableException(getStatusMessage("Service temporarily unavailable; please try again later"));
            case 500 -> throw new ServiceUnavailableException(getStatusMessage("Service unavailable: Internal server error"));
            case 412 -> throw new PreconditionFailedException(getStatusMessage("Precondition failed"), httpResponse);
            case 404 -> throw new ApiException(getStatusMessage("Not found"), httpResponse);
            case 401 -> throw new UnauthorizedException(getStatusMessage("Unauthorized"));
            case 400 ->
                throw ContentType.APPLICATION_JSON.getMimeType().equals(contentType) ? ApiException.createBadRequestJsonException(httpResponse, body)
                        : new ApiException(getStatusMessage("Bad Request"), httpResponse);
            default -> {
                if (httpResponse.getStatusLine().getStatusCode() > 500) {
                    throw new ServiceUnavailableException(
                            getStatusMessage("Service unavailable: Error " + httpResponse.getStatusLine().getStatusCode()));
                }
                if (httpResponse.getStatusLine().getStatusCode() > 400) {
                    throw new ApiException(getStatusMessage("Bad Request"), httpResponse);
                }
            }
            }
            return this;
        }

        @FunctionalInterface
        public interface Parser<R> {
            R apply(Response response) throws InvalidResponseException, ConfigValidationException;
        }

        private String getStatusMessage(String fallback) {
            if (ContentType.APPLICATION_JSON.getMimeType().equals(contentType)) {
                try {
                    Object bodyObj = DocReader.json().read(body);
                    if (bodyObj instanceof Map) {
                        Object errorObj = ((Map<?, ?>) bodyObj).get("error");
                        if (errorObj instanceof String) {
                            return (String) errorObj;
                        }
                        if (errorObj instanceof Map && ((Map<?, ?>) errorObj).get("message") instanceof String) {
                            return (String) ((Map<?, ?>) errorObj).get("message");
                        }
                        Object message = ((Map<?, ?>) bodyObj).get("message");
                        if (message != null) {
                            return message.toString();
                        }
                    }
                } catch (DocumentParseException e) {
                    log.log(Level.WARNING, "Error while parsing JSON response", e);
                }
            }
            String message = httpResponse.getStatusLine().getReasonPhrase();
            return Strings.isNullOrEmpty(message) ? fallback : message;
        }
    }
}
