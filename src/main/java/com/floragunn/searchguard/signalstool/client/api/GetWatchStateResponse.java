package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.codova.documents.DocNode;
import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;
import org.apache.http.entity.ContentType;

import java.util.ArrayList;
import java.util.List;

public class GetWatchStateResponse extends BasicResponse {
    private final String name;
    private final LastStatus lastStatus;
    private final LastExecution lastExecution;
    private final List<Action> actions = new ArrayList<>();

    public GetWatchStateResponse(String name, SearchGuardRestClient.Response response)
            throws SearchGuardRestClient.InvalidResponseException, ConfigValidationException {
        super(response);
        ValidationErrors error = new ValidationErrors();
        ValidatingDocNode state = new ValidatingDocNode(content, error);
        this.name = name;
        lastStatus = state.get("last_status").by(LastStatus::from);
        lastExecution = state.get("last_execution").by(LastExecution::from);
        DocNode actionsNode = state.get("actions").required().asDocNode();
        ValidationErrors actionError = new ValidationErrors(error, "actions");
        actionsNode.toMapOfNodes().forEach((actionName, actionNode) -> {
            try {
                actions.add(Action.from(actionName, actionNode));
            } catch (ConfigValidationException e) {
                actionError.add(actionName, e);
            }
        });
        error.throwExceptionForPresentErrors();
    }

    public GetWatchStateResponse(DocNode content) throws ConfigValidationException {
        super(content, ContentType.APPLICATION_JSON.getMimeType(), null);
        ValidationErrors error = new ValidationErrors();
        ValidatingDocNode node = new ValidatingDocNode(content, error);
        String id = node.get("_id").required().asString();
        name = id.substring(id.indexOf('/') + 1);
        ValidatingDocNode state = node.get("_source").required().asValidatingDocNode();
        lastStatus = state.get("last_status").by(LastStatus::from);
        lastExecution = state.get("last_execution").by(LastExecution::from);
        DocNode actionsNode = state.get("actions").required().asDocNode();
        ValidationErrors actionError = new ValidationErrors(error, "actions");
        actionsNode.toMapOfNodes().forEach((actionName, actionNode) -> {
            try {
                actions.add(Action.from(actionName, actionNode));
            } catch (ConfigValidationException e) {
                actionError.add(actionName, e);
            }
        });
        error.throwExceptionForPresentErrors();
    }

    public String getName() {
        return name;
    }

    public LastStatus getLastStatus() {
        return lastStatus;
    }

    public LastExecution getLastExecution() {
        return lastExecution;
    }

    public List<Action> getActions() {
        return actions;
    }

    public record Action(String name, LastStatus lastStatus, String lastTriggered, boolean acknowledged) {
        public static Action from(String name, DocNode actionNode) throws ConfigValidationException {
            ValidationErrors errors = new ValidationErrors();
            ValidatingDocNode node = new ValidatingDocNode(actionNode, errors);
            LastStatus lastStatus = node.get("last_status").by(LastStatus::from);
            String lastTriggered = node.get("last_triggered").asString();
            boolean acknowledged = node.hasNonNull("acked");
            errors.throwExceptionForPresentErrors();
            return new Action(name, lastStatus, lastTriggered, acknowledged);
        }
    }

    public record LastExecution(Severity severity) {
        public static LastExecution from(DocNode lastExecutionNode) throws ConfigValidationException {
            ValidationErrors errors = new ValidationErrors();
            ValidatingDocNode node = new ValidatingDocNode(lastExecutionNode, errors);
            Severity severity = node.get("severity").by(Severity::from);
            errors.throwExceptionForPresentErrors();
            return new LastExecution(severity);
        }
    }

    public record LastStatus(LastStatus.Code code, String detail) {

        public static LastStatus from(DocNode lastStatusNode) throws ConfigValidationException {
            ValidationErrors errors = new ValidationErrors();
            ValidatingDocNode node = new ValidatingDocNode(lastStatusNode, errors);
            Code code = node.get("code").required().asEnum(Code.class);
            String detail = node.hasNonNull("detail") ? node.get("detail").asString() : null;
            errors.throwExceptionForPresentErrors();
            return new LastStatus(code, detail);
        }

        public enum Code {
            EXECUTION_FAILED, NO_ACTION, ACTION_EXECUTED, SIMULATED_ACTION_EXECUTED, ACTION_FAILED, ACTION_THROTTLED, ACKED
        }
    }

    public record Severity(Level level) {

        public static Severity from(DocNode severityNode) throws ConfigValidationException {
            ValidationErrors errors = new ValidationErrors();
            ValidatingDocNode node = new ValidatingDocNode(severityNode, errors);
            Level level = node.get("level").required().asEnum(Level.class);
            errors.throwExceptionForPresentErrors();
            return new Severity(level);
        }

        public enum Level {
            NONE, INFO, WARNING, ERROR, CRITICAL;

            public String getName() {
                return name().toLowerCase();
            }
        }
    }
}
