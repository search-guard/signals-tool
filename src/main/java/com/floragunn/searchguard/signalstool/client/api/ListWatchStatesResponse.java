package com.floragunn.searchguard.signalstool.client.api;

import com.floragunn.codova.validation.ConfigValidationException;
import com.floragunn.codova.validation.ValidatingDocNode;
import com.floragunn.codova.validation.ValidationErrors;
import com.floragunn.searchguard.signalstool.client.SearchGuardRestClient;

import java.util.List;

public class ListWatchStatesResponse extends BasicResponse {
    private final List<GetWatchStateResponse> watchStateResponses;
    private final long totalHits;

    public ListWatchStatesResponse(SearchGuardRestClient.Response response)
            throws SearchGuardRestClient.InvalidResponseException, ConfigValidationException {
        super(response);
        ValidationErrors errors = new ValidationErrors();
        ValidatingDocNode hits = new ValidatingDocNode(content.getAsNode("hits"), errors);
        ValidatingDocNode total = hits.get("total").required().asValidatingDocNode();
        totalHits = total.get("value").required().asPrimitiveLong();
        watchStateResponses = hits.get("hits").asList(GetWatchStateResponse::new);
        errors.throwExceptionForPresentErrors();
    }

    public List<GetWatchStateResponse> getWatchStateResponses() {
        return watchStateResponses;
    }

    public long getTotalHits() {
        return totalHits;
    }
}
